package hello;


import model.Order;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OrderController {

    private OrderDao dao;

    public OrderController(OrderDao dao) {
        this.dao = dao;
    }

    @PostMapping("orders")
    public Order saveOrder(@RequestBody @Valid Order order) {
        return dao.insertOrder(order);
    }

    @GetMapping("orders")
    public List<Order> getAllOrders() {
        return dao.findAllOrders();
    }

    @GetMapping("orders/{id}")
    public Order getOrderById(@PathVariable Long id) {
        return dao.findOrderByID(id);
    }

    @DeleteMapping("orders/{id}")
    public void deletePost(@PathVariable Long id) {
        dao.deleteOrder(id);
    }


}
